package MinhDuy_Assignment0;

public class Knight {
private int baseHP ;
private int wp ;
public Knight(int baseHP, int wp) {
	if(baseHP < 99 || baseHP > 999 || wp < 0 || wp > 3 )
	   System.out.println("This input is invalid. Please follow the rule");
	else
	{
		this.baseHP = baseHP ;
		this.wp =wp ;
	}
}
public int getBaseHP() {
	return baseHP ;
}
public int getwp() {
	return wp;
}
public int getRealHP() {
	if (wp == 0)
		return baseHP/10 ;
	else 
		return baseHP;
	}
}
